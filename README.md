**Brice Ethuin DevOPS**

_la commande permet de créer le réseau personnalisé :_

gcloud compute networks create taw-custom-network --subnet-mode custom

_Pour créer un sous réseau (exemple Pour subnet-us-west1), il faut taper :_ 

gcloud compute networks subnets create subnet-us-west1 \
   --network taw-custom-network \
   --region us-west1 \
   --range 10.0.0.0/16


_Pour vérifier les réseaux : _

gcloud compute networks subnets list \
--network taw-custom-network


_Créer une regle (exemple http) :_ 

gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http

_créer une instance nommée us-test-01 dans le sous-réseau "subnet-<REGION>" :_

gcloud compute instances create us-test-01 \
--subnet subnet-Region \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules

_Il permet de mesurer la latence :_

ping -c 3 us-test-02.ZONE

La commande pour lister les noms des comptes actifs :

gcloud auth list
 
La commande pour  lister les ID de projet :

gcloud config list project
 
La commande pour ouvrir un nouvel onglet Cloud Shell et vérifiez si Terraform est disponible :

terraform
 
Afin de créer un fichier de configuration vide nommé instance.tf :

touch instance.tf
 
Aller sur le fichier instance.tf et ajoutez-y le contenu suivant (cette configuration sert à déployer une instance de machine virtuelle sur GCP avec des spécifications telles que le type de machine, l'image du système d'exploitation et les paramètres réseau.) :

resource "google_compute_instance" "terraform" {
  project      = ""
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = ""
 
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }
 
   network_interface {
    network = "default"
    access_config {
    }
  }
}
 
 
La commande pour que le fichier qui a été créé a bien été ajouté et que le répertoire ne contient aucun autre fichier *.tf, puisque Terraform charge tous les fichiers stockés dans ce répertoire :

ls
 
Télécharger et installez le binaire du fournisseur :

terraform init
 
Créez un plan d'exécution :

terraform plan
 
Dans le répertoire contenant le fichier instance.tf que vous avez créé, exécutez cette commande :

terraform apply
 
Inspectez l'état actuel :

terraform show
